#lang racket

(define set-of-numbers (set 1 2 3 4 5 6 7 8 9))


(define (area-code area-num)
  (match area-num
    (0 '((0 2) (0 2)))
    (1 '((3 5) (0 2)))
    (2 '((6 8) (0 2)))
    (3 '((0 2) (3 5)))
    (4 '((3 5) (3 5)))
    (5 '((6 8) (3 5)))
    (6 '((0 2) (6 8)))
    (7 '((3 5) (6 8)))
    (8 '((6 8) (6 8)))))


(struct inspection-result (unknown options locations valid) #:transparent)
(define (extract-inspection-result res)
  (values (inspection-result-unknown res)
          (inspection-result-options res)
          (inspection-result-locations res)
          (inspection-result-valid res)))



(define (make-empty-board)
  (let ((row (make-vector 9 'unknown)))
    (make-vector 9 row)))



(define (clone-board board)
  (list->vector (map vector-copy (vector->list board))))



(define (set-cell board x y value)
  (cond [(and (< x 9)
              (< y 9))
         (let* ([new-board (clone-board board)]
                [row (vector-ref new-board y)])
           (vector-set! row x value)
           (vector-set! new-board y row)
           new-board)]
        [else (error (string-append "Invalid cell access on "
                                    (number->string x)
                                    ","
                                    (number->string y)))]))



(define (get-cell board x y)
  (vector-ref (vector-ref board y) x))



(define (print-board board)
  (define (print-row row)
    (for-each (lambda (v) (cond [(eq? v 'unknown) (printf "_")]
                                [else (printf "~a" v)]))
              (vector->list row))
    (newline))
  (cond [(eq? board #f) (displayln "no solution found!")]
        [else (for-each print-row (vector->list board))]))



; making two assumptions here:
; - the file has a correct structure
; - the initial board in the file is actually correct/solvable
;   (as in "not already having doubled numbers in rows/cols/areas")
(define (read-board-from-file filename)
  (define (char->cell c)
    (cond [(char=? c #\space) 'unknown]
          [else (- (char->integer c) (char->integer #\0))]))
  (let ([rows '()])
    (with-input-from-file (string->path filename)
      (lambda ()
        (for ([i (range 9)])
          (set! rows (append rows (list (read-line))))))
      #:mode 'text)
    (list->vector (map (lambda (s) (apply vector (map char->cell
                                                      (string->list s))))
                       rows))))



(define (inspect-board-rows-cols-area board rows cols)
  (let ([options set-of-numbers]
        [unknowns '()])
    (inspection-result
     (foldl (lambda (row col acc)
              (let ([value (get-cell board col row)])
                (cond [(eq? value 'unknown) (set! unknowns (append unknowns (list (cons col row)))) (add1 acc)]
                      [else (set! options (set-subtract options (set value))) acc])))
            0
            rows
            cols)
     options
     unknowns
     (= (set-count options) (length unknowns))))) ; if the number of unknown positions is not equal to the number of options
                                                  ; that means that there is a number doubled in the row, column or area - 
                                                  ; which in turn means that the board is invalid at this point!


(define (list-insert lst value at)
  (cond [(> at (length lst)) (error (format "list-insert -- list not long enough (lst: ~a; at: ~a)" lst at))]
        [(= at (length lst)) (append lst (list value))]
        [(= at 0) (cons value (cons (car lst) (cdr lst)))]
        [else (cons (car lst) (list-insert (cdr lst) value (sub1 at)))]))



(define (make-permutations lst)
  (define (helper lst results)
    (cond [(null? lst) results]
          [else (let ([element (car lst)])
                  (helper (cdr lst)
                          (apply append (map (lambda (result)
                                               (map (lambda (pos) (list-insert result element pos))
                                                    (range (add1 (length result)))))
                                             results))))]))
  (cond [(= 1 (length lst)) (list lst)]
        [else (helper (cdr lst) (list (list (car lst))))]))



(define (carthesian-product list1 list2)
  (let [(res '())]
    (for-each (lambda (l1)
                (for-each (lambda (l2) (set! res (cons (cons l1 l2) res)))
                          list2))
              list1)
    res))



(define (inspect-row board row)
  (let* ([cols (range 9)]
         (rows (make-list (length cols) row)))
    (inspect-board-rows-cols-area board rows cols)))



(define (inspect-col board col)
  (let* ([rows (range 9)]
         (cols (make-list (length rows) col)))
    (inspect-board-rows-cols-area board rows cols)))



(define (inspect-area board area)
  (let* ([area-codes (area-code area)]
         [rows-raw (cadr area-codes)]
         [cols-raw (car area-codes)]
         [rows (range (car rows-raw) (add1 (cadr rows-raw)))]
         [cols (range (car cols-raw) (add1 (cadr cols-raw)))]
         [permutations (carthesian-product rows cols)])
    (inspect-board-rows-cols-area board
                                  (map car permutations)
                                  (map cdr permutations))))



; just for debugging purposes
(define (print-inspection-result board f row-col-area description)
  (let-values ([(unknown-count options unknown-locations valid) (extract-inspection-result (f board row-col-area))])
    (printf "----------~n~a: ~a~n  unknowns: ~a~n  options: ~a~n  locations: ~a~n  valid: ~a~n----------~n"
            description
            row-col-area
            unknown-count
            options
            unknown-locations
            valid)))



(define (count-unknown board)
  (let ([sum 0])
    (for ([y (range 9)])
      (for ([x (range 9)])
        (when (eq? (get-cell board x y) 'unknown)
          (set! sum (add1 sum)))))
    sum))



; no unknown fields left -> done!
; inspect every row, col and area
; when board is invalid -> return #f
; - remember all places with options
; - pick the one with the fewest options
; - create <options> new boards by using permutations
; - recursively call self with new boards
; - when one step returned a board -> done!
; - else return #f
;;; Of course this algorithm can be optimized but "premature optimization is the root of all evil"
(define (solve-board board)
  (define (sort-inspection-results results)
    (filter (lambda (e) (> (inspection-result-unknown e) 0))
            (sort results
                  <
                  #:key inspection-result-unknown)))
  
  (define (board-invalid? row-insp col-insp area-insp)
    (member #f (flatten (append (map inspection-result-valid row-insp)
                                (map inspection-result-valid col-insp)
                                (map inspection-result-valid area-insp)))))
  
  (define (assign-options locations options)
    ; we always have the same amount of options and locations!
    (map (lambda (opts)
           (foldl (lambda (loc opt acc) (cons (cons loc opt) acc))
                  '()
                  locations
                  opts))
         (make-permutations options)))
  
  (define (create-next-board current-board next-options)
    (let ([board current-board])
      (for-each (lambda (option)
                  (set! board (set-cell board (caar option) (cdar option) (cdr option))))
                next-options)
      board))
  
  (define (eval-boards boards)
    (cond [(null? boards) #f]
          [else (let ([res (solve-board (car boards))])
                  (cond [(not (eq? res #f)) res]
                        [else (eval-boards (cdr boards))]))]))
  
  (cond [(= 0 (count-unknown board)) board]
        [else (let ([row-inspection (sort-inspection-results (map (lambda (x) (inspect-row board x)) (range 9)))]
                    [col-inspection (sort-inspection-results (map (lambda (x) (inspect-col board x)) (range 9)))]
                    [area-inspection (sort-inspection-results (map (lambda (x) (inspect-area board x)) (range 9)))])
                (cond [(board-invalid? row-inspection col-inspection area-inspection) #f]
                      [else (let* ([next-try (car (sort-inspection-results (list (car row-inspection)
                                                                                 (car col-inspection)
                                                                                 (car area-inspection))))]
                                   [next-options (assign-options (inspection-result-locations next-try)
                                                                 (set->list (inspection-result-options next-try)))]
                                   [next-boards (map ((curry create-next-board) board) next-options)])
                              
                              (eval-boards next-boards))]))]))



(define (measure-time thunk)
  (let ([start (current-inexact-milliseconds)])
    (thunk)
    (printf "Time: ~a ms~n" (- (current-inexact-milliseconds) start))))

;(let ([b (read-board-from-file "./testboard")])
;  (print-board (solve-board b)))

(let ([b (read-board-from-file "./evil-testboard")])
  (measure-time (lambda () (print-board (solve-board b)))))